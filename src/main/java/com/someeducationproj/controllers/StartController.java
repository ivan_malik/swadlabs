package com.someeducationproj.controllers;

import com.someeducationproj.dao.StudentDAO;
import com.someeducationproj.dao.StudentDAOImpl;
import com.someeducationproj.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class StartController {

    @Autowired
    private StudentDAOImpl studentDAO;

    @RequestMapping("/")
    public ModelAndView handleRequest() throws Exception {
        List<Student> allStudents = studentDAO.findAllStudents();
        ModelAndView model = new ModelAndView("StudentList");
        model.addObject("allStudents", allStudents);
        return model;
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public ModelAndView newStudent() {
        ModelAndView model = new ModelAndView("StudentForm");
        model.addObject("student", new Student());
        return model;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView editUser(HttpServletRequest request) {
        int userId = Integer.parseInt(request.getParameter("id"));
        Student student = studentDAO.findById(userId);
        ModelAndView model = new ModelAndView("StudentForm");
        model.addObject("student", student);
        return model;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView deleteStudent(HttpServletRequest request) {
        int studentId = Integer.parseInt(request.getParameter("id"));
        studentDAO.deleteById(studentId);
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView saveStudent(@ModelAttribute Student student) {
        studentDAO.save(student);
        return new ModelAndView("redirect:/");
    }
}
