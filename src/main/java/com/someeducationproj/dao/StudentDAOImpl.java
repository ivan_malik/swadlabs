package com.someeducationproj.dao;

import com.someeducationproj.model.Student;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import javax.transaction.Transactional;
import java.util.List;

@Repository("studentDAO")
public class StudentDAOImpl implements StudentDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public StudentDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public StudentDAOImpl() {
    }

    @Override
    @Transactional
    public Student findById(int id) {
        String hql = "from Student where id= " + id;
        Query query = sessionFactory.getCurrentSession().createQuery(hql);

        @SuppressWarnings("unchecked")
        List<Student> listUser = (List<Student>) query.list();

        if (listUser != null && !listUser.isEmpty()) {
            return listUser.get(0);
        }

        return null;
    }

    @Override
    @Transactional
    public void save(Student student) {
        sessionFactory.getCurrentSession().saveOrUpdate(student);
    }

    @Override
    @Transactional
    public void deleteById(int id) {
        Student student = new Student();
        student.setId(id);
        sessionFactory.getCurrentSession().delete(student);
    }

    @Override
    @Transactional
    public List<Student> findAllStudents() {
        @SuppressWarnings("unchecked")
        List<Student> listUser = (List<Student>) sessionFactory.getCurrentSession()
                .createCriteria(Student.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

        return listUser;
    }
}
