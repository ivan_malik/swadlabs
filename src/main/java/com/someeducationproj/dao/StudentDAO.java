package com.someeducationproj.dao;


import com.someeducationproj.model.Student;

import java.util.List;

public interface StudentDAO {
    Student findById(int id);

    void save(Student user);

    void deleteById(int id);

    List<Student> findAllStudents();
}

