package com.someeducationproj.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;


public class SEPWebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[] {SEPRootConfig.class};
    }

    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] {SEPWebConfig.class};
    }

    protected String[] getServletMappings() {
        return new String[] {"/"};
    }
}
