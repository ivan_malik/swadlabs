<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Home</title>
</head>
<body>
<div align="center">
    <h1>Students List</h1>
    <h2><a href="/new">New Student</a></h2>

    <table border="1">
        <th>No</th>
        <th>First name</th>
        <th>Last name</th>
        <th>University</th>
        <th>Average mark</th>>

        <c:forEach var="student" items="${allStudents}" varStatus="status">
            <tr>
                <td>${student.id + 1}</td>
                <td>${student.firsName}</td>
                <td>${student.lastName}</td>
                <td>${student.university}</td>
                <td>${student.avgMark}</td>
                <td>
                    <a href="/edit?id=${student.id}">Edit</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="/delete?id=${student.id}">Delete</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>